Rails.application.routes.draw do
  devise_for :users
  namespace :api, defaults: { format: :json }  do
    scope module: :v1 do     
      # We are going to list our resources here
      resources :users,             :only => [:create, :show]
      resources :contacts,          :only => [:create,        :index,  :destroy]
      resources :projects,          :only => [:create, :show, :update, :destroy]
      resources :companies,         :only => [:create,                 :destroy]
      resources :teammembers,       :only => [:create, :show, :update, :destroy]
      resources :tasks,             :only => [:create, :show, :update, :destroy]
      resources :departments,       :only => [:create, :show, :update, :destroy]
      resources :projectmembers,    :only => [:create, :show, :update, :destroy]
      resources :timeentries,       :only => [:create]
      resources :manageusers,       :only => [:index]
    end
  end 
      match '/api/socialaccount'                                => 'api/v1/sessions#createSessionsSocialAccount'  ,via: [ :post] 
      match '/api/sessions/login'                               => 'api/v1/sessions#create'                       ,via: [ :post]
      match '/api/sessions/logout'                              => 'api/v1/sessions#destroy'                      ,via: [ :delete]
      
      match '/api/manageusers/chooseevent'                      => 'api/v1/manageusers#chooseEvent'               ,via: [ :post]

      match '/api/delete_feedbacks'                             => 'api/v1/contacts#delete_feedbacks'             ,via: [ :post]
      match '/api/reply_feedback'                               => 'api/v1/contacts#reply_feedback'               ,via: [ :post]

      match '/api/deletecompanies'                              => 'api/v1/companies#delete_companies'            ,via: [ :post]
      match '/api/managecompanies'                              => 'api/v1/companies#departments_and_companies'   ,via: [ :post]
      match '/api/update_company_departments'                   => 'api/v1/companies#update_company_departments'  ,via: [ :post]

      match '/api/users/forgotpassword'                         => 'api/v1/users#generate_new_password_email'     ,via: [ :post]
      match '/api/invite_members'                               => 'api/v1/users#invite_members'                  ,via: [ :post]
      match '/api/update_team_member'                           => 'api/v1/users#update_team_member'              ,via: [ :post]
      match '/api/send_notification_for_inviting'               => 'api/v1/users#send_notification_for_inviting'  ,via: [ :post]
      match '/api/get_answer_from_notification'                 => 'api/v1/users#get_answer_from_notification'    ,via: [ :post]
      match '/api/change_password'                              => 'api/v1/users#change_password'                 ,via: [ :post]
      match '/api/create_subscribe'                             => 'api/v1/users#create_subscribe'                ,via: [ :post]
      match '/api/get_subscribe_emails'                         => 'api/v1/users#get_subscribe_emails'            ,via: [ :get]
      match '/api/delete_subscribe_mail/:id'                    => 'api/v1/users#delete_subscribe_mail'           ,via: [ :delete]
      match '/api/delete_subscribe_mails'                       => 'api/v1/users#delete_subscribe_mails'          ,via: [ :post]
      match '/api/send_new_info_to_subscribe_mails'             => 'api/v1/users#send_new_info_to_subscribe_mails',via: [ :post]

      match '/api/manage_projects_members'                      => 'api/v1/projects#manage_projects_members'      ,via: [ :post]
      match '/api/projects_of_user'                             => 'api/v1/projects#projects_of_user'             ,via: [ :post]
      match '/api/projects_of_user1'                            => 'api/v1/projects#projects_of_user1'            ,via: [ :post]
      match '/api/delete_projects'                              => 'api/v1/projects#delete_projects'              ,via: [ :post]
      match '/api/update_project'                               => 'api/v1/projects#update_project'               ,via: [ :post]
      match '/api/values_of_new_project'                        => 'api/v1/projects#values_of_new_project'        ,via: [ :post]

      match '/api/projectmembers/makeprojectmanager'            => 'api/v1/projectmembers#make_project_manager'   ,via: [ :post]
      match '/api/delete_project_members'                       => 'api/v1/projectmembers#delete_project_members' ,via: [ :post]

      match '/api/deletetasks'                                  => 'api/v1/tasks#delete_tasks'                    ,via: [ :post]
      match '/api/deletetask'                                   => 'api/v1/tasks#delete_task'                     ,via: [ :post]
      match '/api/task_timeentries'                             => 'api/v1/tasks#task_timeentries'                ,via: [ :post]
      match '/api/start_task_time_entry'                        => 'api/v1/tasks#start_task_time_entry'           ,via: [ :post]
      match '/api/stop_task_time_entry'                         => 'api/v1/tasks#stop_task_time_entry'            ,via: [ :post]
      match '/api/resume_task_time_entry'                       => 'api/v1/tasks#resume_task_time_entry'          ,via: [ :post]
      match '/api/deletetimeentries'                            => 'api/v1/tasks#delete_time_entries'             ,via: [ :post]
      match '/api/deletetimeentry'                              => 'api/v1/tasks#delete_time_entry'               ,via: [ :post]
      match '/api/task_timeentries_are_sorted'                  => 'api/v1/tasks#task_timeentries_are_sorted'               ,via: [ :post]

      match '/api/conpanies_of_user'                            => 'api/v1/teammembers#companies_of_user'         ,via: [ :get]
      match '/api/members_of_company'                           => 'api/v1/teammembers#members_of_company'        ,via: [ :post]
      match '/api/delete_team_members'                          => 'api/v1/teammembers#delete_team_members'       ,via: [ :post]  
      
      match '/api/owned_companies_departments'                  => 'api/v1/departments#owned_companies_departments',via: [ :post]
      match '/api/report'                                       => 'api/v1/reports#report_v2'                         ,via: [:post]          
      # Email confirmable route
      # devise_scope :user do
      #   match '/confirm/:confirmation_token', :to => "api/v1/mails#confirm_user", :as => "user_confirm", :only_path => false, via: [:get]
      # end
end
