desc 'send reminder email'
task send_reminder_email: :environment do
	users = User.all
			users.each do |user|
				UserMailer.remind_users(user).deliver_now!
			end
end