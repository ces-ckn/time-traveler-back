class CreateProjectmembers < ActiveRecord::Migration
  def change
    create_table :projectmembers do |t|

      t.belongs_to :project, index: true
      t.belongs_to :teammember, index: true
      t.integer :project_manager, default: 0
      t.integer :active, default: 1
    end
  end
end
