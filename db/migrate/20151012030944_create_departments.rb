class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|

      t.belongs_to :company, index: true
      t.string :department_name
    end
  end
end
