class CreateTimeentries < ActiveRecord::Migration
  def change
    create_table :timeentries do |t|
    	t.belongs_to :task, index: true
    	t.datetime :start	 
    	t.datetime :end
    end
  end
end
