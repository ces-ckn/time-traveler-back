class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :subject
      t.string :email
      t.string :message
      t.integer :reply, default: 0

    end
  end
end
