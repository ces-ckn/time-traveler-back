class CreateTaskUsers < ActiveRecord::Migration
  def change
    create_table :task_users do |t|
    	t.belongs_to :user, index: true
    	t.belongs_to :task, index: true

    end
  end
end
