class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
    	t.belongs_to :project, index: true
    	t.string :task_name

    end
  end
end
