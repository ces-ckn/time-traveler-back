class CreateTeammembers < ActiveRecord::Migration
  def change
    create_table :teammembers do |t|
      t.belongs_to :department, index: true
      t.belongs_to :user, index: true
      t.string     :email
      t.belongs_to :company, index: true
      t.integer    :verify, default: 0
      t.integer    :company_ad, default: 0
    end
  end
end
