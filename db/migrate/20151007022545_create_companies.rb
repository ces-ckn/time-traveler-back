class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.belongs_to :user, index: true
      t.string :company_name
      
    end
  end
end
