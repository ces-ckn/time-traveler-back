class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :project_name
      t.belongs_to :company, index: true
      t.string :client_name
      t.string :color
      t.string :desc
      
    end
  end
end
