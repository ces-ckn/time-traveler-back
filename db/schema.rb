# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151130043948) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "companies", force: :cascade do |t|
    t.integer "user_id"
    t.string  "company_name"
  end

  add_index "companies", ["user_id"], name: "index_companies_on_user_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "subject"
    t.string   "email"
    t.string   "message"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "reply",      default: 0
  end

  create_table "departments", force: :cascade do |t|
    t.integer "company_id"
    t.string  "department_name"
  end

  add_index "departments", ["company_id"], name: "index_departments_on_company_id", using: :btree

  create_table "projectmembers", force: :cascade do |t|
    t.integer "project_id"
    t.integer "teammember_id"
    t.integer "project_manager", default: 0
    t.integer "active",          default: 1
  end

  add_index "projectmembers", ["project_id"], name: "index_projectmembers_on_project_id", using: :btree
  add_index "projectmembers", ["teammember_id"], name: "index_projectmembers_on_teammember_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string  "project_name"
    t.integer "company_id"
    t.string  "client_name"
    t.string  "color"
    t.string  "desc"
  end

  add_index "projects", ["company_id"], name: "index_projects_on_company_id", using: :btree

  create_table "subscribes", force: :cascade do |t|
    t.string "email"
  end

  create_table "task_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "task_users", ["task_id"], name: "index_task_users_on_task_id", using: :btree
  add_index "task_users", ["user_id"], name: "index_task_users_on_user_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.integer "project_id"
    t.string  "task_name"
  end

  add_index "tasks", ["project_id"], name: "index_tasks_on_project_id", using: :btree

  create_table "teammembers", force: :cascade do |t|
    t.integer "department_id"
    t.integer "user_id"
    t.string  "email"
    t.integer "company_id"
    t.integer "verify",        default: 0
    t.integer "company_ad",    default: 0
  end

  add_index "teammembers", ["company_id"], name: "index_teammembers_on_company_id", using: :btree
  add_index "teammembers", ["department_id"], name: "index_teammembers_on_department_id", using: :btree
  add_index "teammembers", ["user_id"], name: "index_teammembers_on_user_id", using: :btree

  create_table "timeentries", force: :cascade do |t|
    t.integer  "task_id"
    t.datetime "start"
    t.datetime "end"
  end

  add_index "timeentries", ["task_id"], name: "index_timeentries_on_task_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",                   null: false
    t.string   "encrypted_password",     default: "defaultpassword123", null: false
    t.integer  "role",                   default: 0
    t.integer  "status",                 default: 1
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,                    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "auth_token"
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
