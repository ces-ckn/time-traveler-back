class Task < ActiveRecord::Base
	belongs_to :project
	has_many :timeentries, dependent: :destroy
	has_many :task_users, dependent: :destroy

	# def remind_users_about_tasks
	# 		users = User.all
	# 		users.each do |user|
	# 			UserMailer.remind_users(user.email).deliver!
	# 		end
	# end
end
