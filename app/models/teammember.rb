class Teammember < ActiveRecord::Base
	belongs_to :joined_company, foreign_key: :company_id, class_name: "Company"
	belongs_to :user, foreign_key: :user_id
	belongs_to :department, foreign_key: :department_id

	JOIN = 1
  	DECLINE = 2
  	UNCONFIRMED = 0

  	COMPANYADMIN = 1
  	COMPANYMEMBER = 0

end
