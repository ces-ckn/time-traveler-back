class Company < ActiveRecord::Base
	has_many :projects, dependent: :destroy
	has_many :teammembers, dependent: :destroy
	has_many :departments, dependent: :destroy
	# has_many :members, through: :teammembers
	belongs_to :user

end
