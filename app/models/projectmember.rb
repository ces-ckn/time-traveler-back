class Projectmember < ActiveRecord::Base
	belongs_to :project, foreign_key: :project_id 
	belongs_to :teammember
	PROJECTMANAGER = 1
end
