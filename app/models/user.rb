class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # validates :auth_token, uniqueness: true

  devise :database_authenticatable, :registerable,:recoverable, :rememberable, :trackable, :validatable, :confirmable
  before_create :generate_authentication_token!
  has_many :companies
  has_many :joined_companies, through: :teammembers, :class_name => "Company"
  has_many :teammembers
  has_many :task_users
  has_many :tasks, through: :task_users, :class_name => "Task"


  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
  end

  ADMIN = 1
  USER = 0

  DELETE = 1
  STATUS = 2
  MAKEADMIN = 3

  BLOCK = 0
  UNBLOCK = 1
end
