class Api::V1::ProjectmembersController < Api::V1::DefaultController
	respond_to :json

    def make_project_manager
      project_id = project_member_params["project_id"]
      teammember_id = project_member_params["teammember_id"]
      projectMember = Projectmember.where(project_id: project_id, teammember_id: teammember_id).first
      if projectMember.present?
        begin
          projectMember.update_attributes( project_manager: Projectmember::PROJECTMANAGER)
          render json: projectMember, status: 200
        rescue  => e
          render json: { errors: e.message}
        end
      else
        render json: {message: "Member not found"}
      end     
    end

    def delete_project_members
      begin
        project_members = Projectmember.find(delete_project_members_params["ids"])
        project_members.each do |project_member|
            project_member.destroy
        end
        render json: {message: "Deleted successfully"}
      rescue  => e
        render json: {errors: e.message}
      end
    end
    

	private	

    def project_member_params
      params.require(:make_project_maneger).permit(:project_id, :teammember_id )
    end

    def delete_project_members_params
        params.require(:delete_project_members).permit( :ids => []) 
    end
end
