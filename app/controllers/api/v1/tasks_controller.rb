class Api::V1::TasksController < Api::V1::DefaultController
  respond_to :json


    def delete_time_entries
      begin
        time_entries = Timeentry.find(delete_time_entries_params["ids"])
        time_entries.each do |time_entry|
            num_of_entries = Timeentry.where(task_id: time_entry.task_id).count        
            if num_of_entries == 1
              Task.find(time_entry.task_id).destroy
            else
              time_entry.destroy
            end  
        end
        render json: {message: "Delete successfully"}
      rescue  => e
        render json: {errors: e.message}
      end
    end

    def delete_time_entry
      begin
        time_entry = Timeentry.find(delete_time_entry_params["id"])
        num_of_entries = Timeentry.where(task_id: time_entry.task_id).count        
        if num_of_entries == 1
          Task.find(time_entry.task_id).destroy
        else
          time_entry.destroy
        end
        render json: {message: "Delete successfully"}
      rescue  => e
        render json: {errors: e.message}
      end
    end

    def delete_task
      begin
        Task.find(delete_task_params["id"]).destroy
        render json: {message: "Delete successfully"}
      rescue  => e
        render json: {errors: e.message}
      end
    end

    def task_timeentries
      current_user  = User.where(auth_token: auth_token_param[:auth_token]).first
      task_users    = current_user.task_users
      task_ids      = task_users.pluck(:task_id)
      tasks         = Task.where(id: task_ids)

      project_ids = tasks.pluck(:project_id)
      project_ids.delete(nil)
      projects    = Project.find(project_ids)

      hash = {projects: projects,
              :tasks => ActiveModel::ArraySerializer.new(tasks)
              } 
      respond_to do |format|
          format.json { render json: hash}
      end 
    end

    def task_timeentries_are_sorted
      current_user  = User.where(auth_token: auth_token_param[:auth_token]).first
      task_users    = current_user.task_users
      task_ids      = task_users.pluck(:task_id)
      tasks         = Task.where(id: task_ids)

      project_ids = tasks.pluck(:project_id)
      project_ids.delete(nil)
      projects    = Project.find(project_ids)
      time_entries = Timeentry.where(task_id: task_ids)

      hash = {projects: projects,
              tasks: tasks,
              time_entries: time_entries
              } 
      respond_to do |format|
          format.json { render json: hash}
      end
    end

    def start_task_time_entry
      task = Task.new(start_task_time_entry_params["task"])
      current_user = User.where(auth_token: start_task_time_entry_params[:auth_token]).first
      begin
        task.save
        TaskUser.create(user_id: current_user.id, task_id: task.id)
        time_entry = Timeentry.create(task_id: task.id, start: start_task_time_entry_params[:start])
        hash = {task: task,
                time_entry: time_entry
              } 
        respond_to do |format|
          format.json { render json: hash}
        end 
      rescue  => e
        render json: { errors: e.message }
      end
    end

    def resume_task_time_entry
      begin
        time_entry = Timeentry.create(task_id: resume_task_time_entry_params[:task_id], start: resume_task_time_entry_params[:start])
        task = Task.find(time_entry.task_id)
        hash = {task: task,
                time_entry: time_entry
              } 
        respond_to do |format|
          format.json { render json: hash}
        end 
      rescue  => e
        render json: { errors: e.message }
      end
      
    end

    def stop_task_time_entry
      task = Task.find(stop_task_time_entry_params[:task_id])
      time_entry = Timeentry.find(stop_task_time_entry_params[:timeentry_id])
      begin
        task.update(stop_task_time_entry_params[:task])
        time_entry.end = stop_task_time_entry_params[:end]
        time_entry.save!
        project_id = task.project_id
        if project_id.nil?
          hash = {task: task,
                  time_entry: time_entry
              }
        else
          project = Project.find(project_id)
          hash = {task: task,
                  time_entry: time_entry,
                  project: project
              }
        end
        respond_to do |format|
          format.json { render json: hash}
        end
      rescue  => e
        render json: { errors: e.message }
      end  
    end
    

	private

    def delete_time_entries_params
        params.require(:delete_time_entries).permit( :ids => []) 
    end

    def delete_time_entry_params
        params.require(:delete_time_entry).permit( :id) 
    end

    def delete_task_params
        params.require(:delete_task).permit( :id) 
    end

    def resume_task_time_entry_params
      params.require(:resume_task_time_entry).permit( :start , :task_id)             
    end

    def auth_token_param
      params.require(:get_tasks).permit(:auth_token)
    end

    def start_task_time_entry_params 
      params.require(:start_task_time_entry).permit(:auth_token, :start , task: [:project_id, :task_name])             
    end

    def stop_task_time_entry_params 
      params.require(:stop_task_time_entry).permit( :timeentry_id, :end , :task_id, task: [:project_id, :task_name])             
    end

    
end
