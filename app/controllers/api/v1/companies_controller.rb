class Api::V1::CompaniesController < Api::V1::DefaultController
	respond_to :json

  	def create
      current_user = User.where(auth_token: company_params[:auth_token]).first
  			begin 
          company = Company.create(user_id: current_user.id, company_name: company_params[:company_name])
          Teammember.create(user_id: current_user.id, company_id: company.id,verify: Teammember::JOIN, company_ad: Teammember::COMPANYADMIN)
  				department_names = company_params[:department_names]
          if !department_names.empty?
            department_names.each do |department_name|
              Department.create(company_id: company.id, department_name: department_name)
            end
          end
          
          render json: {message: "Company created successfully"}, status: 200
  			rescue => e
  				render json: { errors: e.message}
  			end
  	end

    def update_company_departments

      company = Company.find(update_company_params[:company_id])
      begin
        company.update_attributes( company_name: update_company_params[:company_name])
        new_departments = update_company_params["department_names"]
        old_departments = Department.where(company_id: update_company_params[:company_id]).pluck(:department_name)

        if !new_departments.empty?
            new_departments.each do |new_department|
              if !new_department.in? (old_departments)
                 Department.create(company_id: update_company_params[:company_id], department_name: new_department) 
              end
            end
        end

        if !old_departments.empty?
            old_departments.each do |old_department|
              if !old_department.in? (new_departments)
                  department = Department.where(company_id: update_company_params[:company_id], department_name: old_department).first
                  teammembers = Teammember.where(department_id: department.id)
                  teammembers.each do |teammembers|
                    teammembers.update_attributes( department_id: nil)
                  end
                  Department.where(company_id: update_company_params[:company_id], department_name: old_department).first.destroy 
              end
            end
        end
        render json: {message: "Updated successfully"}, status: 200
      rescue  => e
        render json: { errors: e.message }
      end

    end

  	def departments_and_companies
      current_user = User.where(auth_token: auth_token_param[:auth_token]).first
  		companies = current_user.companies
    	departments = Department.where(company_id: companies.ids)
    	hash = {companies: companies,departments: departments}
    	respond_to do |format|
   	 	  format.json { render json: hash}
    	end
  	end

    def destroy
      begin
        Company.find(params["id"]).destroy
        render json: {message: "Successfully deleted Company"}, status: 200
      rescue  => e
        render json: { errors: e.message }
      end
    end

    def delete_companies
      begin
        companies = Company.find(companies_params["ids"])
        companies.each do |company|
              company.destroy
        end
          render json: {message: "Successfully deleted"}
        rescue  => e
          render json: {errors: e.message}
      end
    end

	  private
    
      def company_params
        params.require(:create_company).permit(:auth_token, :company_name, :department_names => []) 
      end

      def update_company_params
        params.require(:update_company).permit(:company_id, :company_name, :department_names => []) 
      end

      def auth_token_param
        params.require(:get_companies).permit(:auth_token)
      end

      def companies_params
          params.require(:delete_companies).permit( :ids => []) 
      end
    
end
