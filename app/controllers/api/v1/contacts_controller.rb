class Api::V1::ContactsController < ApplicationController
	respond_to :json

	def create		
		begin
		 	contact = Contact.create(contact_params)	
			render json: {message:"Your message is already sent"} 
		rescue => e
      		render json: {errors: e.message }
		end	
	end

	def index
		begin
			feedbacks = Contact.all 
      		respond_to do |format|
      			format.json { render json: feedbacks}
    		end
		rescue  => e
			render json: {errors: e.message }
		end
    end

	def destroy
		begin
			Contact.find(params["id"]).destroy
			render json: {message:"Deleted successfully!"}
		rescue  => e
			render json: {errors: e.message }
		end
	end

	def delete_feedbacks
		begin
        	feedbacks = Contact.find(delete_feedbacks_params["ids"])
        	feedbacks.each do |feedback|
            	feedback.destroy
        	end
        	render json: {message: "Delete successfully"}
      	rescue  => e
        	render json: {errors: e.message}
      	end
	end

	def reply_feedback
		begin
			feedback = Contact.find(reply_feedback_params["feedback_id"])
			message = reply_feedback_params["message"]
			email = feedback.email
			feedback.update_attributes( reply: Contact::REPLIED)
			ContactMailer.reply_feedback(email,message).deliver
			render json: {message: "Sent successfully"}
		rescue  => e
			render json: {errors: e.message}
		end
	end

	private	
		def contact_params	
			params.require(:contact).permit(:subject, :email, :message)	
		end

		def delete_feedbacks_params
			params.require(:delete_feedbacks).permit( :ids => [])
		end

		def reply_feedback_params
			params.require(:reply_feedback).permit(:feedback_id, :message)
		end
end
