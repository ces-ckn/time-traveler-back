class Api::V1::DepartmentsController < Api::V1::DefaultController
	respond_to :json

    def owned_companies_departments
        current_user = User.where(auth_token: auth_token_params[:auth_token]).first

        owned_companies =  current_user.companies
        joined_companies_ids = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER).pluck(:company_id)
        joined_companies = Company.find(joined_companies_ids)
        owned_departments  = Department.where(company_id: owned_companies.ids)

        hash = {
                owned_companies:  owned_companies,
                joined_companies:  joined_companies,
                owned_departments: owned_departments
                } 

        respond_to do |format|
            format.json { render json: hash}
        end
    end

	private

    def auth_token_params
        params.require(:get_departments).permit(:auth_token)
    end

end
