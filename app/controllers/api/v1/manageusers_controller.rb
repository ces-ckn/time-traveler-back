class Api::V1::ManageusersController < Api::V1::DefaultController
  respond_to :json
  # before_create :generate_authentication_token!
  
    def index
    	users = User.all 
      respond_to do |format|
      	format.json { render :json => users.to_json( :only => [:id, :email,:role, :status, :updated_at] ) }
    	end
    end

    def chooseEvent
      begin
          users = User.find(id_params["ids"])
          users.each do |user|
            if user.role == User::USER
                if user.status == User::UNBLOCK
                  user.update_attributes( status: User::BLOCK)
                else
                  user.update_attributes( status: User::UNBLOCK)
                end    
            end    
          end
          render json: {message:"Successfully actived/unactived!"}
      rescue  => e
          render json: { errors: e.message}
      end
    end

  private 

    def id_params  
      params.require(:user).permit( :event, :ids => []) 
    end    
    
end

