class Api::V1::ReportsController < Api::V1::DefaultController
	respond_to :json


        def report_v2

            begin
                current_user = User.where(auth_token: auth_token_param[:auth_token]).first

                #companies that current user owned and joined
                owned_companies = current_user.companies
                owned_projects = Project.where(company_id: owned_companies.ids)

                owned_tasks_of_member = Task.where(project_id: owned_projects.ids)

                task_users    = current_user.task_users
                tasks_of_cur  = Task.where(id: task_users.pluck(:task_id))

                teammembers = Teammember.where(company_id: owned_companies.ids, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER)

                #//////////////////////////////////////////////////////////////////////////
                joined_companies_ids = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER).pluck(:company_id)
                joined_companies = Company.find(joined_companies_ids)

                teammembers_of_cur = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER)
                managed_projects_ids = Projectmember.where(teammember_id: teammembers_of_cur.ids, project_manager: Projectmember::PROJECTMANAGER).pluck(:project_id)
                managed_projects = Project.where(id: managed_projects_ids)

                managed_tasks_of_member = Task.where(project_id: managed_projects.ids)
                managed_projectmembers = Projectmember.where(project_id: managed_projects_ids, project_manager: 0)

                hash = {
                  :owned_companies => ActiveModel::ArraySerializer.new(owned_companies),
                  :owned_tasks_of_member =>  ActiveModel::ArraySerializer.new(owned_tasks_of_member),
                  :teammembers =>   ActiveModel::ArraySerializer.new(teammembers),
                  :tasks_of_cur => ActiveModel::ArraySerializer.new(tasks_of_cur),
                  :joined_companies =>   ActiveModel::ArraySerializer.new(joined_companies),
                  :joined_managed_projects =>   ActiveModel::ArraySerializer.new(managed_projects),
                  :joined_managed_projectmembers =>   ActiveModel::ArraySerializer.new(managed_projectmembers),
                  :joined_managed_tasks_of_member =>   ActiveModel::ArraySerializer.new(managed_tasks_of_member)
                }
                respond_to do |format|
                  format.json { render json: hash}
                end
            rescue  => e
                render json: {errors: e.message}
            end     
        end

    private
    
	   def auth_token_param
            params.require(:get_report).permit(:auth_token)
       end
end
