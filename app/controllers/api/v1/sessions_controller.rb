class Api::V1::SessionsController < Api::V1::DefaultController
	respond_to :json	

  	def create 
      user_password = params[:session][:password]
      user_email = params[:session][:email]
      remember = params[:session][:remember_me]
      user = User.find_by(email: user_email)
      begin
        if user && user.valid_password?(user_password)
          if user.status == User::UNBLOCK
            sign_in(user)
            if remember
              user.remember_me!
            end 
            render json: user
          else
            render json: {errors: "Your account is blocked"}
          end
        else
          render json: {errors: "Invalid email or password"}
        end 
      rescue => e
        render json: {errors: e.message}
      end
    end

    def createSessionsSocialAccount
      user_email = social_user_params["email"]
      begin
        if user_email.present? && User.find_by(email: user_email)
          user = User.find_by(email: user_email)
          if user.status == User::UNBLOCK
            sign_in(user)
            render json: user
          else
            render json: {errors: "Your account is blocked"}
          end
        else
          user = User.create(social_user_params)
          user.update_attributes( confirmed_at: user.confirmation_sent_at)
          teammember = Teammember.where(email: user.email).first
          if teammember.present?
              teammembers = Teammember.where(email: user.email)
              teammembers.each do |member|
                member.update_attributes( user_id: user.id, email: nil)
              end
          end
          sign_in(user)
          render json: user
        end
      rescue  => e
        render json: {errors: e.message}
      end
      
    end

    def destroy
      user = User.find_by(auth_token: params[:auth_token])
      if user
        sign_out(user)
        user.generate_authentication_token!
        user.save
        render json: {message: "Log out successfully"}
      else
        render json: {errors: "No user to log out"}
      end
    end

  private

    def social_user_params
      params.require(:user).permit(:email,:password,:password_confirmation)
    end

end
