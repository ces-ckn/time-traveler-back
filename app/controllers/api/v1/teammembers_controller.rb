class Api::V1::TeammembersController < Api::V1::DefaultController
  respond_to :json
  
    def companies_of_user
      current_user = User.where(auth_token: auth_token_param[:auth_token]).first
      joined_companies_ids = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER).pluck(:company_id)
      joined_companies = Company.find(joined_companies_ids)
      companies_own = current_user.companies
      
      respond_to do |format|
      format.json { render json: { companies_own:  companies_own,
                                    all_companies:  joined_companies
                                  }
                  }
       end 
    end

    def members_of_company
      current_user = User.where(auth_token: get_team_members_params[:auth_token]).first

      joined_companies_ids = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER).pluck(:company_id)
      owned_companies = current_user.companies

      owned_team_members = Teammember.where(company_id: owned_companies.ids)
      joined_team_members = Teammember.where(company_id: joined_companies_ids)
      
      respond_to do |format|
      format.json { render json: { :owned_team_members => ActiveModel::ArraySerializer.new(owned_team_members),
                                    :joined_team_members => ActiveModel::ArraySerializer.new(joined_team_members)
                                  }
                  }
       end   
    end

    def delete_team_members
      begin
        team_members = Teammember.find(delete_team_members_params["ids"])
        team_members.each do |team_member|
            if team_member.user_id.nil?
              team_member.destroy
            elsif team_member.verify == Teammember::UNCONFIRMED
              team_member.destroy
            else
              company_id = team_member.company_id
              projects_ids_of_company = Project.where(company_id: company_id).ids
              projects_ids_of_project_member = Projectmember.where(teammember_id: team_member.id).pluck(:project_id)
              projects_ids = projects_ids_of_company & projects_ids_of_project_member
              if projects_ids.empty?
                team_member.destroy
              else
                task_ids = TaskUser.where(user_id: team_member.user_id).pluck(:task_id)
                projects_ids.each do |projects_id|
                  task_ids.each do |task_id|
                    if Task.find(task_id).project_id == projects_id
                        Task.find(task_id).destroy
                    end
                  end
                  Projectmember.find(projects_id).destroy
                end
                team_member.destroy
              end
            end
        end
        render json: {message: "Delete successfully"}
      rescue  => e
        render json: {errors: e.message}
      end
    end

	private

    def auth_token_param
      params.require(:get_companies).permit(:auth_token)
    end

    def get_team_members_params
      params.require(:get_team_members).permit(:auth_token)
    end

    def delete_team_members_params
        params.require(:delete_team_members).permit( :ids => []) 
    end

end
