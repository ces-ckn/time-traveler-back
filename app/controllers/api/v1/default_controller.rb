class Api::V1::DefaultController < ApplicationController
	# before_action :authentication

	def authentication
		# 401 Unauthorized
		render json: { errors: "Unauthorized"} unless authentication_user
	end

	def authentication_user
		params[:auth_token] && User.find_by_auth_token(params[:auth_token]).present?
	end
end