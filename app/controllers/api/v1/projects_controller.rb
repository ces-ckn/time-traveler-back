class Api::V1::ProjectsController < Api::V1::DefaultController
	respond_to :json

  	def create
      begin
        current_user = User.where(auth_token: create_project_params[:auth_token]).first
        project = Project.create(create_project_params[:project])
        project_manager = Teammember.where(user_id: current_user.id,company_id: project.company_id).first
        Projectmember.create(project_id: project.id, teammember_id: project_manager.id, project_manager: Projectmember::PROJECTMANAGER)
        teammember_ids = create_project_params["teammember_ids"]
        if !teammember_ids.empty?
            teammember_ids.each do |teammember_id|
              Projectmember.create(project_id: project.id, teammember_id: teammember_id)
            end
        end  
        render json: {message: "Project created successfully"}, status: 200
      rescue  => e
        render json: { errors: e.message }
      end
  	end

    def values_of_new_project
      begin
        current_user = User.where(auth_token: values_of_new_project_params[:auth_token]).first
        joined_companies_ids = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER).pluck(:company_id)
        joined_companies = Company.find(joined_companies_ids)
        owned_companies  = current_user.companies  

        joined_teammembers = Teammember.where(company_id: joined_companies_ids,verify: Teammember::JOIN)
        owned_teammembers  = Teammember.where(company_id: owned_companies.ids,verify: Teammember::JOIN)

        joined_clients = Project.where(company_id: joined_companies_ids).pluck(:company_id, :client_name).uniq
        joins = joined_clients.map do |joined_client|
            joined_clients_hash = {
              company_id: joined_client[0],
              client_name: joined_client[1]
            }
        end
        
        owned_clients = Project.where(company_id: owned_companies.ids).pluck(:company_id, :client_name).uniq
        owns = owned_clients.map do |owned_client|
            owned_clients_hash = {
              company_id: owned_client[0],
              client_name: owned_client[1]
            }
        end

        hash = {
          joined_companies: joined_companies,
          owned_companies: owned_companies,
          :joined_teammembers =>  ActiveModel::ArraySerializer.new(joined_teammembers),
          :owned_teammembers =>   ActiveModel::ArraySerializer.new(owned_teammembers),
          joined_clients_name: joins,
          owned_clients_name: owns
        }
        respond_to do |format|
          format.json { render json: hash}
        end
      rescue  => e
        render json: { errors: e.message }
      end 

    end

	  def update_project
    	project = Project.find(update_project_params["project_id"])
      begin
        project.update(update_project_params[:project])
        new_teammember_ids = update_project_params["teammember_ids"]

        p 11111111111111111111
        p new_teammember_ids
        p 222222222222222222222222
        old_teammember_ids = Projectmember.where(project_id: update_project_params["project_id"]).pluck(:teammember_id)
        

        p 33333333333333333333
        p  old_teammember_ids
        p 444444444444444444444 

        if !new_teammember_ids.empty?
            new_teammember_ids.each do |new_teammember_id|
              if !new_teammember_id.to_i.in? (old_teammember_ids)
                 Projectmember.create(project_id: project.id, teammember_id: new_teammember_id) 
              end
            end
        end

        if !old_teammember_ids.empty?
            old_teammember_ids.each do |old_teammember_id|
              if !old_teammember_id.to_s.in? (new_teammember_ids)
                  task_ids = TaskUser.where(user_id: old_teammember_id.user_id).pluck(:task_id)
                  task_ids.each do |task_id|
                    if Task.find(task_id).project_id == project.id
                        Task.find(task_id).destroy
                    end
                  end
                 Projectmember.where(project_id: project.id, teammember_id: old_teammember_id).first.destroy 
              end
            end
        end
        render json: {message: "Updated successfully"}, status: 200
      rescue  => e
        render json: { errors: e.message }
      end
  	end  	

    def projects_of_user
      begin
          current_user = User.where(auth_token: auth_token_param[:auth_token]).first
        
          joined_companies_ids = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER).pluck(:company_id)
          joined_companies = Company.find(joined_companies_ids)
          companies_own = current_user.companies
        
          respond_to do |format|
          format.json { render json: { :companies_own => ActiveModel::ArraySerializer.new(companies_own),
                                       :all_companies => ActiveModel::ArraySerializer.new(joined_companies)
                                     }
                                   }
          end
      rescue  => e
        render json: { errors: e.message}
      end
      
    end

    def projects_of_user1
      begin
        current_user = User.where(auth_token: auth_token_param[:auth_token]).first
        teammember_ids = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN).ids

        project_ids = Projectmember.where(teammember_id: teammember_ids).pluck(:project_id)

        project_ids.delete(nil)

        projects = Project.where(id: project_ids)

        # project.compact

        company_ids = projects.pluck(:company_id)

        companies = Company.where(id: company_ids)

        value = companies.map do |company|
          value_hash = {
            company: {
                      id: company.id,
                      name: company.company_name,
                      projects: projects_value = projects.select{ |i| i.company_id == company.id }.map { |i|
                          i
                      }                        
            }
          }
        end  

        respond_to do |format|
          format.json { render json: { projects: value                                        
                                     }
                                   }
          end
      rescue  => e
        render json: { errors: e.message}
      end
      
    end

    def manage_projects_members
      begin
          current_user = User.where(auth_token: auth_token = auth_token_param[:auth_token]).first

          joined_companies_ids = Teammember.where(user_id: current_user.id, verify: Teammember::JOIN, company_ad: Teammember::COMPANYMEMBER).pluck(:company_id)
          joined_companies = Company.find(joined_companies_ids)
          owned_companies =  current_user.companies 

          joined_projects = Project.where(company_id: joined_companies_ids)
          owned_projects  = Project.where(company_id: owned_companies.ids)

          joined_projectmembers = Projectmember.where(project_id: joined_projects.ids)
          owned_projectmembers = Projectmember.where(project_id: owned_projects.ids)

          hash = {
                  :joined_companies => ActiveModel::ArraySerializer.new(joined_companies),
                  :owned_companies => ActiveModel::ArraySerializer.new(owned_companies),
                  :joined_projectmembers => ActiveModel::ArraySerializer.new(joined_projectmembers),
                  :owned_projectmembers => ActiveModel::ArraySerializer.new(owned_projectmembers)
                  # :joined_companies => ActiveModel::ArraySerializer.new(joined_companies, scope: { has_teammember: true}, each_serializer: CompanySerializer)
                  } 

          respond_to do |format|
              format.json { render json: hash}
          end
      rescue  => e
        render json: { errors: e.message}
      end
                                                                      

    end

    def delete_projects
      begin
        projects = Project.find(delete_projects_param["ids"])
        projects.each do |project|
            project.destroy
        end
        render json: {message: "Delete successfully"}
      rescue  => e
        render json: {errors: e.message}
      end
    end

    def destroy
      begin
        Project.find(params["id"]).destroy
        render json: {message: "Deleted successfully"}, status: 200
      rescue  => e
        render json: { errors: e.message}
      end     
    end

	private	

    def create_project_params 
      params.require(:create_project).permit(:auth_token, project: [:project_name, :company_id, :client_name, :color, :desc], :teammember_ids => [])             
    end

    def update_project_params 
      params.require(:update_project).permit(:project_id, project: [:project_name, :client_name, :color, :desc], :teammember_ids => [])             
    end

    def auth_token_param
      params.require(:get_companies).permit(:auth_token)
    end

    def delete_projects_param
        params.require(:delete_projects).permit( :ids => []) 
    end

    def values_of_new_project_params
      params.require(:get_values_of_new_project).permit(:auth_token)
    end

end
