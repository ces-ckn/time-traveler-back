class Api::V1::UsersController < Api::V1::DefaultController
  respond_to :json
  
    def create
      begin
        user = User.create(user_params)
        teammember = Teammember.where(email: user_params[:email]).first
        if teammember.present?
              teammembers = Teammember.where(email: user_params[:email])
              teammembers.each do |member|
                member.update_attributes( user_id: user.id, email: nil)
              end
        end
        render json: {message: "Sign up successfully"}
      rescue => e
        render json: { errors: e.message }
      end
    end

    def generate_new_password_email
      begin
        user = User.where(email: params[:email]).first
        length = 8
        chars = 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'
        new_password = ''
        length.times{new_password << chars[rand(chars.size)]}
        user.update_attribute(:password, new_password)
        UserMailer.send_new_password(user.email,new_password).deliver
        render json: {message: "Reset password instructions have been sent to #{user.email}"}
      rescue => e
        render json: {errors: e.message}
      end
    end

    def invite_members
      email = invite_members_params[:email]
      exist_email = User.all.pluck(:email)
      if email.in? (exist_email)
          begin
            user_id = User.where(email: email).first.id

            invited_member_in_this_company = Teammember.where(user_id: user_id, company_id: invite_members_params[:company_id]).first
            if invited_member_in_this_company.present?
                render json: {message: "Already invited"}
            else
                if !invite_members_params[:department_id].empty?
                  teammember = Teammember.create(company_id: invite_members_params[:company_id], user_id: user_id, department_id: invite_members_params[:department_id])
                else
                  teammember = Teammember.create(company_id: invite_members_params[:company_id], user_id: user_id)
                end
                UserMailer.invite_member(email).deliver
                render json: {message: "Invited successfully"}
            end  
          rescue  => e
            render json: {errors: e.message}
          end 
      else
          begin
            invited_member_in_this_company = Teammember.where(email: email, company_id: invite_members_params[:company_id]).first
            if invited_member_in_this_company.present?
              render json: {message: "Already invited"}
            else
              teammember = Teammember.create(invite_members_params)
              UserMailer.invite_member(email).deliver
              render json: {message: "Invited successfully"}
            end   
          rescue  => e
            render json: {errors: e.message}
          end
      end
    end

    def update_team_member
      teammember = Teammember.find(update_team_member_params[:teammember_id])
      begin
        teammember.update_attributes(department_id: update_team_member_params[:teammember][:department_id])
        render json: {message: "Updated successfully"}
      rescue  => e
        render json: {errors: e.message}
      end
    end

    def send_notification_for_inviting
      current_user = User.where(auth_token: send_notification_params[:auth_token]).first
      begin
        teammember = Teammember.where(user_id: current_user.id, verify: Teammember::UNCONFIRMED).first
        if teammember.present?
              teammembers = Teammember.where(user_id: current_user.id, verify: Teammember::UNCONFIRMED)
              company_ids = teammembers.pluck(:company_id)
              companies = Company.find(company_ids)
              hash = ActiveModel::ArraySerializer.new(companies, scope: { has_project: false}, each_serializer: CompanySerializer)  
              
              respond_to do |format|
                  format.json { render json: hash}
              end
        else
          render json: {message: "No invitation"}  
        end
      rescue  => e
        render json: {errors: e.message}
      end
      
    end

    def get_answer_from_notification
      current_user = User.where(auth_token: answer_team_member_params[:auth_token]).first
      begin
        teammembers = Teammember.where(user_id: current_user.id, company_id: answer_team_member_params[:company_id])
        if answer_team_member_params[:answer].to_i == Teammember::JOIN
          teammembers.each do |teammember|
            teammember.update_attributes(verify: Teammember::JOIN)
          end
        else
          teammembers.each do |teammember|
            teammember.destroy
          end
        end
        render json: {message: "Got answer successfully"}
      rescue  => e
        render json: {errors: e.message}
      end
    end

    def change_password
      current_user = User.where(auth_token: change_password_params[:auth_token]).first
      begin
        if current_user.update_attributes( password: change_password_params[:new_password],password_confirmation: change_password_params[:password_confirmation] )
          render json: {message: "Your password was updated successfully"}
        else
          render json: {message: "No updated"}
        end
      rescue  => e
        render json: {errors: e.message}
      end
    end

    def create_subscribe
      begin
        sub_emails =  Subscribe.all
        if !subscribe_params[:email].in? (sub_emails)
          Subscribe.create(subscribe_params) 
        end
        render json: {message: "OK"}
      rescue  => e
        render json: {errors: e.message}
      end
    end

    def get_subscribe_emails
      begin
        emails = Subscribe.all 
        respond_to do |format|
          format.json { render json: emails}
        end
      rescue  => e
        render json: {errors: e.message }
      end
    end

    def delete_subscribe_mail
    begin
      Subscribe.find(params["id"]).destroy
      render json: {message:"Deleted successfully!"}
    rescue  => e
      render json: {errors: e.message }
    end
  end

  def delete_subscribe_mails
    begin
          subscribe_mails = Subscribe.find(delete_subscribe_mails_params["ids"])
          subscribe_mails.each do |subscribe_mail|
              subscribe_mail.destroy
          end
          render json: {message: "Delete successfully"}
        rescue  => e
          render json: {errors: e.message}
        end
  end

  def send_new_info_to_subscribe_mails
    begin
      subscribe_mails = Subscribe.find(send_new_info_to_subscribe_mails_params["ids"])
      message = send_new_info_to_subscribe_mails_params["message"]
      subscribe_mails.each do |subscribe_mail|
            SubscribeMailer.send_new_info_to_subscribe_mail(subscribe_mail.email,message).deliver  
      end
      render json: {message: "Sent successfully"}
    rescue  => e
      render json: {errors: e.message}
    end
  end

  private

    def send_new_info_to_subscribe_mails_params
      params.require(:send_new_info_to_subscribe_mails).permit(:message, :ids => [] )
    end

    def delete_subscribe_mails_params
      params.require(:delete_subscribe_mails).permit( :ids => [])
    end

    def subscribe_params
      params.require(:subscribe).permit(:email)
    end

    def answer_team_member_params
      params.require(:answer_of_team_member).permit(:auth_token, :company_id, :answer)
    end
  
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end

    def change_password_params
      params.require(:user).permit(:auth_token, :new_password, :password_confirmation)
    end

    def invite_members_params
      params.require(:invite_member).permit(:email, :company_id, :department_id)
    end

    def update_team_member_params
      params.require(:update_team_member).permit(:teammember_id, teammember: [:department_id, :company_id])
    end

    def send_notification_params
      params.require(:get_notification).permit(:auth_token)
    end

    def send_new_password_params
      params.require(:user).permit(:email)
    end
end