class Devise::ConfirmationsController < DeviseController
	def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token])
    yield resource if block_given?

    if resource.errors.empty?
      set_flash_message(:notice, :confirmed) if is_flashing_format?
      # respond_with_navigational(resource){ redirect_to "http://time-traveler-front.herokuapp.com" }
      redirect_to "http://time-traveler-front.herokuapp.com"
    else
      respond_with_navigational(resource.errors, status: :unprocessable_entity){ render :new }
    end
  end
end
