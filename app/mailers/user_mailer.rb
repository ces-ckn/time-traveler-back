class UserMailer < ApplicationMailer
	def invite_member(email)
    	mail(:to => email, :subject => "Invite Member", :body => "Greetings,\n\nYou are invited to someone's projects on Time Travelers.\n\nPlease log in to see the details at : time-traveler-front.herokuapp.com\n\nIf you don't have any accounts, feel free to create one.\n\nCheers,\nTime Travelers")
  	end
  	def remind_users(user)
  		@cur_user = user
		mail(:to => @cur_user.email, 
			 :subject => "Reminder", 
			 :body => "Test reminder")
	end
	def send_new_password(email,pass)
		mail(
			:to => email,
			:subject => "Your new password",
			:body => "Greetings,\n\nThis is your new password : #{pass}\n\nYou can change it later if you want.\n\nCheers,\nTime Travelers"
			)
	end
end

