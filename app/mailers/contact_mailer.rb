class ContactMailer < ApplicationMailer
	def reply_feedback(email,message)
    	mail(:to => email, :subject => "Reply Feedback", :body => message)
  	end
end
