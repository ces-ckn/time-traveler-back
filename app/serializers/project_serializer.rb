class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :project_name, :client_name, :color, :desc, :company_id, :company_name
  
  # customize attributes
  def attributes
    super.merge(customize_attributes)
  end

  # customize attributes
  def customize_attributes
    attribute_values = {}
    attributes = attr_methods = []

    attr_methods += [:projectmembers] if @scope.blank? || @scope[:has_projectmembers]

    attributes.each{|u| attribute_values.merge!({u => object[u]})}
    attr_methods.each{|u| attribute_values.merge!({ u => self.send(u)})}
    attribute_values
  end

  def company_name
    Company.find(company_id).company_name
  end

  def projectmembers
  	object.projectmembers
  end
end
