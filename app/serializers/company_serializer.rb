class CompanySerializer < ActiveModel::Serializer
  attributes :id, :user_id, :email, :company_name

  # customize attributes
  def attributes
    super.merge(customize_attributes)
  end

  # customize attributes
  def customize_attributes
    attribute_values = {}
    attributes = attr_methods = []
    attr_methods += [:projects] if @scope.blank? || @scope[:has_project]

    attributes.each{|u| attribute_values.merge!({u => object[u]})}
    attr_methods.each{|u| attribute_values.merge!({ u => self.send(u)})}
    attribute_values
  end

  # def projects
  # 	ActiveModel::ArraySerializer.new(object.projects, scope: @scope, each_serializer: ProjectSerializer)
  # end

  def projects
    object.projects
  end

  def email
  	User.find(user_id).email
  end
end


