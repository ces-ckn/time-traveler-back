class ProjectmemberSerializer < ActiveModel::Serializer
  attributes :id, :project_id, :teammember_id, :email, :project_manager, :company_id

  def email
  	if teammember_id.nil?
  		nil
  	elsif	!Teammember.find(teammember_id).user_id.nil?
  		User.find(Teammember.find(teammember_id).user_id).email
  	else
  		Teammember.find(teammember_id).email
  	end
  end

  def company_id
    Project.find(project_id).company_id
  end

end
