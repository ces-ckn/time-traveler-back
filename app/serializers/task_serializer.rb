class TaskSerializer < ActiveModel::Serializer
  attributes :id, :project_id,:project_name, :task_name, :email 
  has_many :timeentries
  
  def email    
    User.find(TaskUser.where(task_id: id).first.user_id).email
  end

  def project_name
  	if project_id.nil?
  		nil
  	else
  		Project.find(project_id).project_name
  	end
  	
  end
  
end
