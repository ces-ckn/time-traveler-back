class TeammemberSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :email, :company_id, :company_admin_id, :company_name, :department_id, :department_name, :verify, :company_ad

  def company_name
    if company_id.nil?
      nil
    else
      Company.find(company_id).company_name
    end
  end

  def department_name
  	if department_id.nil?
  		nil
  	else
  		Department.find(department_id).department_name
  	end
  end

  def email
    if !user_id.nil?
      User.find(user_id).email
    else
      Teammember.find(id).email
    end
    
  end

  def company_admin_id
    if company_id.nil?
      nil
    else
      Company.find(company_id).user_id
    end
  end

end
